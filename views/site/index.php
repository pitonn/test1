<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\ActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <?php try {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'category',
                    'filter' => \yii\helpers\ArrayHelper::map(\app\models\Category::find()->all(), 'id', 'name'),
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        if ($model->categories !== null) {
                            return implode(', ', array_column($model->categories, 'name'));
                        }
                        return null;
                    }
                ],
                [
                    'attribute' => 'title',
                    'filter' => false,
                ],
                [
                    'attribute' => 'text',
                    'filter' => false,
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
    } catch (Exception $e) {
    } ?>

</div>
