<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Action */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="action-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category[]')->dropDownList(\app\models\Category::listCategory(), [
        'value' => array_column($model->categories, 'id'),
        'multiple' => true,
    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput(['value' => $model->created_at ? Yii::$app->formatter->asDate($model->created_at, 'MM/d/y') : null, 'disabled' => true]) ?>

    <?= $form->field($model, 'valid_until')->widget(\kartik\date\DatePicker::class, [
        'options' => [
            'value' => $model->valid_until ? Yii::$app->formatter->asDate($model->valid_until, 'MM/d/y') : null,
        ],
        'pluginOptions' => [
//            'format' => 'mm/dd/yyyy',
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
