<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%action}}".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property int $created_at
 * @property int $valid_until
 *
 * @property LinkActionCategory[] $linkActionCategories
 */
class Action extends \yii\db\ActiveRecord
{
    public $category = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%action}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text', 'valid_until'], 'required'],
            [['text'], 'string'],
            [['created_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['category', 'valid_until'], 'safe'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'created_at' => Yii::t('app', 'Created At'),
            'valid_until' => Yii::t('app', 'Valid Until'),
        ];
    }

    /**
     * Все связи
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLinkActionCategories()
    {
        return $this->hasMany(LinkActionCategory::class, ['action_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\ActionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ActionQuery(get_called_class());
    }

    /**
     * Все категории
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])
            ->via('linkActionCategories');
    }

    public function getCategory(int $id)
    {
        return $this->getCategories()->andFilterWhere(['category_id' => $id]);
    }

    public function beforeSave($insert)
    {
        // связь action -> category
        foreach ($this->categories as $category) {
            // если нет в новых данных
            if (!in_array($category->id, $this->category)) {
                // если есть связь, то удаляем
                if (($link = LinkActionCategory::find()->where(['action_id' => $this->id, 'category_id' => $category->id])->one()) !== null) {
                    $link->unlink('action', $this, true);
                }
            }
        }

        // преобразовываем дату
        $this->valid_until = strtotime($this->valid_until);
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        // связь action -> category
        foreach ($this->category as $category) {
            // если не существует, то делаем новую
            if (($link = LinkActionCategory::find()->where(['action_id' => $this->id, 'category_id' => $category])->one()) === null) {
                $link = new LinkActionCategory(['category_id' => (int)$category]);
                $link->link('action', $this);
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }
}
